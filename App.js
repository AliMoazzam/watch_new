import React from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';

import Timer from './components/Timer'
import RoundButton from './components/RoundButton'
import ButtonsRow from './components/ButtonsRow'
import LapsTable from './components/LapsTable'


export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      start: 0,
      now: 0,
      laps: []
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  start = () => {
    const now = new Date().getTime();
    this.setState({
      start: now,
      now,
      laps: [0],
    })

    this.timer = setInterval(() => {
      this.setState({ now: new Date().getTime() })
    }, 100);
  }

  lap = () => {
    const timestamp = new Date().getTime()
    const { laps, now, start } = this.state
    const [firstLap, ...other] = laps
    this.setState({
      laps: [0, firstLap + now - start, ...other],
      start: timestamp,
      now: timestamp
    })
  }
  
  stop = () => {
    clearInterval(this.timer);
    const { laps, now, start } = this.state
    const [firstLap, ...other] = laps
    this.setState({
      laps: [firstLap + now - start, ...other],
      start: 0,
      now: 0,
    })
  }

  reset = () => {
    this.setState({
      laps: [],
      start: 0,
      now: 0
    })
  }

  resume = () => {
    const now = new Date().getTime()
    this.setState({
      start: now,
      now,
    })

    this.timer = setInterval(() => {
      this.setState({ now: new Date().getTime() })
    }, 100);
  }

  render() {
    const { now, start, laps } = this.state
    const timer = now - start
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <Timer
          interval={laps.reduce((total, curr) => total + curr, 0) + timer}
          style={styles.timer}
        />
        {laps.length === 0 && (
          <ButtonsRow>
            <RoundButton
              title="Start"
              color="#50D167"
              background="#1B361F"
              onPress={this.start}
            />
            <RoundButton
              color="#8B8B90"
              title="Lap"
              background="#151515"
              onPress={this.reset}
              disabled 
            />
          </ButtonsRow>
        )}
        {start > 0 && (
          <ButtonsRow>
            <RoundButton
              color="#E33935"
              title="Stop"
              background="#3C1715"
              onPress={this.stop}
            />
            <RoundButton
              color="#50D167"
              background="#1B361F"
              title="Lap"
              onPress={this.lap}
            />
          </ButtonsRow>
        )}
        {laps.length > 0 && start === 0 && (
          <ButtonsRow>
            <RoundButton
              color="#E33935"
              title="Reset"
              background="#3C1715"
              onPress={this.reset}
            />
            <RoundButton
              color="#50D167"
              background="#1B361F"
              title="Start"
              onPress={this.resume}
            />
          </ButtonsRow>
        )}
        <LapsTable laps={laps} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0d0d0d',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 100,
    paddingHorizontal: 20,
  },
  timer: {
    color: '#fff',
    fontSize: 76,
    fontWeight: '100',
    width: 110,
  },  
});
