import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

import Timer from './Timer'

function Lap({ number, interval, fastest, slowest }) {

  const lapStyle = [
    styles.lapText,
    fastest && styles.fastest,
    slowest && styles.slowest
  ]
  return (
    <View style={styles.laps}>
      <Text style={lapStyle}>Lap {number}</Text>
      <Timer style={[lapStyle, styles.lapTimer]} interval={interval} />
    </View>
  )
}

const styles = StyleSheet.create({
  laps: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: '#151515',
    borderTopWidth: 1,
    paddingVertical: 10,
  },
  lapTimer: {
    width: 30,
  },

  lapText: {
    color: '#FFFFFF',
    fontSize: 18,
  },

  fastest: {
    color: '#4BC05F',
  },
  slowest: {
    color: '#CC3531',
  },
})


export default Lap
